@ECHO OFF
@setlocal enableextensions
@cd /d "%~dp0"
echo Installing necessary components...
pip install -r requirements.txt
pause