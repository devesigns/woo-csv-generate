import time
import csv
import sys
import os
import boto3
from slugify import slugify

IMAGE_DIR = os.getcwd() + '/mockup'
ERROR_IMAGE_DIR = os.getcwd() + '/error-images'

aws_credentials = { 
    'aws_access_key_id': 'AKIAIA6FIATVKNJP6NZA',
    'aws_secret_access_key': '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'
}
s3_client = boto3.client('s3', **aws_credentials)
bucket = 'shirt-mockup'
style_url = 'https://s3.us-east-2.amazonaws.com/shirt-mockup/style.jpg'
size_url = 'https://s3.us-east-2.amazonaws.com/shirt-mockup/size.jpg'

try:
    # Read data that needs processing from CSV files
    template = []
    with open('template.csv', newline='') as f:
        reader = csv.reader(f)
        for row in reader:
            if (row[0] == ''):
                continue

            template.append(row)

    # Variables holding index for each field of the data
    indexList = {
        'skuId': -1,
        'nameId': -1,
        'parentId': -1,
        'imagesId': -1
    }

    # First row is header
    # Appending index of each field to the dictionary
    headers = template[0]
    for i in range(0, len(headers)):

        if (headers[i].lower().strip() == 'sku'):
            indexList['skuId'] = i
            continue
            
        if (headers[i].lower().strip() == 'name'):
            indexList['nameId'] = i
            continue

        if (headers[i].lower().strip() == 'parent'):
            indexList['parentId'] = i
            continue
            
        if (headers[i].lower().strip() == 'images'):
            indexList['imagesId'] = i
            continue
        
    # Check if there's any missing fields
    indexErrorList = []
    for key, value in indexList.items():
        if (value == -1):
            indexErrorList.append(key)

    if (len(indexErrorList) > 0):
        sys.exit('Missing fields: ' + str(indexErrorList))

    # List mockup files in mockup folder
    # Many thanks to stackoverflow
    mockupFolderPath = './mockup'
    mockupFiles = [f for f in os.listdir(mockupFolderPath) if os.path.isfile(os.path.join(mockupFolderPath, f))]
    mockupFiles.sort()
    num_mockupFiles = len(mockupFiles)

    with open('import.csv', 'w', newline='\n') as f:
        writer = csv.writer(f, delimiter = ',')
        writer.writerow(template[0])

        mockupId = 0
        while (mockupId < num_mockupFiles):
            try:
                # Beginning from row 2, skip header of the template
                # Processing row 2 as the variable product first

                # There will be 3 mockups for each product, therefore:
                imageNames = []
                # imageNames.append(mockupFiles[mockupId + 2])
                imageNames.append(mockupFiles[mockupId])
                # imageNames.append(mockupFiles[mockupId + 1])

                # print('{}, {}/{}'.format(image_name, mockupId + 1, num_mockupFiles))
                template[1][indexList['nameId']] = imageNames[0].split('.')[0]

                # Use current time in microsecond as the SKU
                parentSku = int(time.time() * 1000000)
                template[1][indexList['skuId']] = str(parentSku)

                # Upload image to Amazon S3 and write image url to variable
                imageUrls = []
                for imageId in range(0, 1):
                    image_path = IMAGE_DIR + '/' + imageNames[imageId]
                    image_key = slugify(imageNames[imageId].split('.')[0]) + '.' +  imageNames[imageId].split('.')[1]
                    print(imageNames[imageId], '\n', mockupId)
                    s3_client.upload_file(image_path, bucket, slugify(image_key))
                    imageUrls.append('https://s3.us-east-2.amazonaws.com/{}/{}'.format(bucket, image_key))
                    
                template[1][indexList['imagesId']] = '{},{},{}'.format(imageUrls[0], style_url, size_url)
                writer.writerow(template[1])
                                                
                # Loop through the variant rows
                for rowId in range(2, len(template)):
                    template[rowId][indexList['nameId']] = imageNames[0].split('.')[0]
                    template[rowId][indexList['parentId']] = parentSku

                    writer.writerow(template[rowId])

            except:
                os.rename(IMAGE_DIR + '/' + imageNames[imageId], ERROR_IMAGE_DIR + '/' + imageNames[imageId])

            mockupId = mockupId + 1 
            
except Exception as ex:
    print(str(ex))
finally:
    print('Completed')
